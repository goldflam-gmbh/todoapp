package de.goldflamgmbh.demo.repository;

import de.goldflamgmbh.demo.model.entity.Todo;
import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Long> {
}