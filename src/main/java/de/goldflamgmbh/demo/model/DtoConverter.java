package de.goldflamgmbh.demo.model;

import de.goldflamgmbh.demo.model.dto.TodoDto;
import de.goldflamgmbh.demo.model.entity.Todo;
import org.springframework.stereotype.Component;

@Component
public class DtoConverter {

    public static TodoDto toDto(Todo todo) {
        return new TodoDto(todo.getId(), todo.getName(), todo.getDone());
    }

    public static Todo toEntity(TodoDto dto) {
        Todo todo = new Todo();
        todo.setId(dto.getId());
        todo.setName(dto.getName());
        todo.setDone(dto.getDone());
        return todo;
    }
}
