package de.goldflamgmbh.demo.controller;

import de.goldflamgmbh.demo.model.DtoConverter;
import de.goldflamgmbh.demo.model.entity.Todo;
import de.goldflamgmbh.demo.model.dto.TodoDto;
import de.goldflamgmbh.demo.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping(produces = "application/json")
    public List<TodoDto> getTodoList() {
        List<Todo> todoList = todoService.findAll();
        return todoList.stream().map(DtoConverter::toDto).toList();
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDto createToDo(@RequestBody TodoDto newItem) {
        Todo todo = DtoConverter.toEntity(newItem);
        Todo savedTodo = todoService.save(todo);
        return DtoConverter.toDto(savedTodo);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public TodoDto getTodo(@PathVariable long id) {
        Todo todo = todoService.findById(id);
        return DtoConverter.toDto(todo);
    }

    @PatchMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public TodoDto updateTodo(@PathVariable long id, @RequestBody TodoDto todoItem) {
        Todo existingTodo = todoService.findById(id);
        existingTodo.setName(todoItem.getName());
        existingTodo.setDone(todoItem.getDone());
        Todo savedTodo = todoService.save(existingTodo);
        return DtoConverter.toDto(savedTodo);
    }
}
