package de.goldflamgmbh.demo.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.goldflamgmbh.demo.model.entity.Todo;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link Todo}
 */
public class TodoDto implements Serializable {

    @JsonProperty("id")
    private Long id = null;
    @JsonProperty("name")
    private String name = null;
    @JsonProperty("done")
    private Boolean done = null;

    public TodoDto() {}

    public TodoDto(String name, Boolean done) {
        this.name = name;
        this.done = done;
    }

    public TodoDto(Long id, String name, Boolean done) {
        this.id = id;
        this.name = name;
        this.done = done;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getDone() {
        return done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoDto entity = (TodoDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.name, entity.name) &&
                Objects.equals(this.done, entity.done);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, done);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ", " +
                "done = " + done + ")";
    }
}