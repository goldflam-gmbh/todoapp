package de.goldflamgmbh.demo.service;

import de.goldflamgmbh.demo.model.entity.Todo;
import de.goldflamgmbh.demo.repository.TodoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> findAll() {
        return (List<Todo>) todoRepository.findAll();
    }

    public Todo findById(long id) {
        return todoRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "todo item not found"));
    }

    public Todo save(Todo todoItem) {
        return todoRepository.save(todoItem);
    }

}
